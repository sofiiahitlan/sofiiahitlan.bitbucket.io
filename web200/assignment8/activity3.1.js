//This program uses a for loop to generate a list of multiplication expressions for a value entered by user. Expressions are added to the array before and being displayed.

"use strict";

debugger;

main();

window.addEventListener("load", function () {
    document.getElementById("value").addEventListener("input", main);
    document.getElementById("expressionsNumber").addEventListener("input", main);
});

function main() {
    let value = getValue();
    let expressionsNumber = getExpressionsNumber();
    let expressions = processExpressions(value, expressionsNumber);
}

function getValue(){
    let value = document.getElementById("value").value;
    // let value = 5;

    return value;
}

function getExpressionsNumber(){
    let expressionsNumber = document.getElementById("expressionsNumber").value;
    // let  expressionsNumber = 9;

    return expressionsNumber;
}

function processExpressions(value, expressionsNumber){
    let expressions = [];
    for (let condition = 0; condition < Number(expressionsNumber); condition++) {
        expressions.push(value.toString() + " * " + condition.toString() + " = " + (value*condition).toString());
    }
    document.getElementById("expressions").innerText = expressions.join("\n\n");        


}
