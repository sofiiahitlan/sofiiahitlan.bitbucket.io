//This program calculates and displays amount of flooring needed for the room in square yards.
//User has to enter length and width of the room in feet.

debugger;

var length = window.prompt("Please a length of your room in feet:");
var width = window.prompt("Please enter a width of your room in feet:");

var squareFeet = length * width;
var squareYards = squareFeet / 9;

console.log("The amount of floor covering you will need for your room is " + squareYards.toFixed(2) + " square yards.");

