//This program calculates and displays weekly, monthly and annual gross pay. 
//User is required to enter his or her rate per hour and amount of hours he or she works every week.

debugger;

var rate = window.prompt("Please enter your hourly rate:");
var hours = window.prompt("Please enter a number of hours you work every week:");

var weekly = rate * hours;
var monthly = weekly * 4;
var annual = monthly * 12;

console.log("Your weekly gross pay is $" + weekly.toFixed(2));
console.log("Your monthly gross pay is $" + monthly.toFixed(2));
console.log("Your annual gross pay is $" + annual.toFixed(2));