//This program uses functions to calculate and display the amount of flooring needed for the room in square yards.
//User has to enter length and width of the room in feet.

"use strict";

debugger;

main();

function main()
{
    let length = getLength();
    let width = getWidth();
    let squareFeet = calculateSquareFeet(length, width);
    let squareYards = calculateSquareYards(squareFeet);
    displayResult(squareYards);
}

function getLength()
{
    let length = window.prompt("Please a length of your room in feet:");

    return length;
}

function getWidth()
{
    let width = window.prompt("Please enter a width of your room in feet:");

    return width;
}

function calculateSquareFeet(length, width)
{
    let squareFeet = length * width;

    return squareFeet;
}

function calculateSquareYards(squareFeet)
{
    let squareYards = squareFeet / 9;

    return squareYards;
}

function displayResult(squareYards)
{
    console.log("You will need " + squareYards.toFixed(2) + " square yards of floor covering for your room.");
}