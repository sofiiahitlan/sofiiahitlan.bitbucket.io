//This program uses functions to calculates and displays weekly, monthly and annual gross pay. 
//User is required to enter his or her rate per hour and amount of hours he or she works every week.

"use strict";

debugger;

main();

function main()
{
    let rate = getRate();
    let hours = getHours();
    let weekly = calculateWeekly(rate, hours);
    let monthly = calculateMonthly(weekly);
    let annual = calculateAnnual(monthly);
    displayResults(weekly, monthly, annual);
}

function getRate() 
{
    let rate = window.prompt("Please enter your hourly rate:");

    return rate;
}

function getHours()
{
    let hours = window.prompt("Please enter a number of hours you work every week:");

    return hours;
}

function calculateWeekly(rate, hours)
{
    let weekly = rate * hours;

    return weekly;
}

function calculateMonthly(weekly)
{
    let monthly = weekly * 4;
    
    return monthly;
}

function calculateAnnual(monthly)
{
    let annual = monthly * 12;

    return annual;
}

function displayResults(weekly, monthly, annual)
{
console.log("Your weekly gross pay is $" + weekly.toFixed(2));
console.log("Your monthly gross pay is $" + monthly.toFixed(2));
console.log("Your annual gross pay is $" + annual.toFixed(2));
}