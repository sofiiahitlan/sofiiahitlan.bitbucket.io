//This program asks user to use a date picker to choose a time. Chosen year, month and day are then displayed separately.
"use strict";

window.addEventListener("load", function () {
    document.getElementById("chosenDateID").addEventListener("input", main);    
  });

main();

function main() {
    //debugger;
    let chosenDate = getChosenDate();
    displayYear(chosenDate);
    displayMonth(chosenDate);
    displayDate(chosenDate);
    displayDayOfTheWeek(chosenDate);
}

function getChosenDate(){
    let value = document.getElementById("chosenDateID").value;
    let chosenDate = new Date(value);

    return chosenDate;
}

function displayYear(chosenDate) {
    let year = chosenDate.getFullYear();
    document.getElementById("year").innerHTML = year;
}

function displayMonth(chosenDate) {
    let month = chosenDate.getMonth();
    let monthsArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    document.getElementById("month").innerHTML = monthsArray[month];
}

function displayDate(chosenDate) {
    let date = chosenDate.getDate();
    document.getElementById("date").innerHTML = date;
}

function displayDayOfTheWeek(chosenDate) {
    let daysArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let dayOfTheWeek = daysArray[chosenDate.getDay()];
    document.getElementById("dayOfTheWeek").innerHTML = dayOfTheWeek;
}



