//This program gets a current time and displays each property seperately.

"use strict";

debugger;

window.addEventListener("load", function () {
    window.setInterval(main, 1000);    
  });

main();

function main() {
    let dateFullString = getDateFullString();
    displayYear(dateFullString);
    displayMonth(dateFullString);
    displayDate(dateFullString);
    displayDayOfTheWeek(dateFullString);
    displayHour(dateFullString);
    displayMinute(dateFullString);
    displaySecond(dateFullString);
}

function getDateFullString(){
let dateFullString = new Date();

return dateFullString;
}

function displayYear(dateFullString) {
    let year = dateFullString.getFullYear();
    document.getElementById("year").innerHTML = year;

    //return year;
}

function displayMonth(dateFullString) {
    let month = dateFullString.getMonth();
    let monthsArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    document.getElementById("month").innerHTML = monthsArray[month];

    //return month;
}

function displayDate(dateFullString) {
    let date = dateFullString.getDate();
    document.getElementById("date").innerHTML = date;

    //return date;
}

function displayDayOfTheWeek(dateFullString) {
    let daysArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let dayOfTheWeek = daysArray[dateFullString.getDay()];
    document.getElementById("dayOfTheWeek").innerHTML = dayOfTheWeek;

    //return dayOfTheWeek;

}

function displayHour(dateFullString) {
    let hour = dateFullString.getHours();
    let hoursArray = [12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    document.getElementById("hour").innerHTML = hoursArray[hour];
    if (hour >= 0 && hour <= 11) {
        document.getElementById("amOrPm").innerHTML = "a.m.";
    } else {
        document.getElementById("amOrPm").innerHTML = "p.m.";
    }
    
    //return hour;
}

function displayMinute(dateFullString) {
    let minute = dateFullString.getMinutes();
    document.getElementById("minute").innerHTML = minute;

    //return minute;
}

function displaySecond(dateFullString) {
    let second = dateFullString.getSeconds();
    document.getElementById("second").innerHTML = second;

    //return second;
}


