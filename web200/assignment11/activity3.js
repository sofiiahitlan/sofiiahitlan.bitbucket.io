// This program gets and displays current window size, screen size, and location information. Window size changes when user resizes the window.

"use strict";
//debugger;

window.addEventListener("load", function () {
    displayWindowSize();
    displayScreenSize();
    displayLocation(); 
  });

window.addEventListener('resize', displayWindowSize);


function displayWindowSize() {
    let windowInnerWidth = window.innerWidth;
    let windowInnerHeight = window.innerHeight;

    document.getElementById("windowInnerWidth").innerText = windowInnerWidth;
    document.getElementById("windowInnerHeight").innerText = windowInnerHeight;

}

function displayScreenSize() {
    let screenWidth = screen.width;
    let screenHeight = screen.height;

    document.getElementById("screenWidth").innerText = screenWidth;
    document.getElementById("screenHeight").innerText = screenHeight;

}

function displayLocation() {
    let locationHost = location.hostname;
    let locationHref = location.href;

    document.getElementById("locationHost").innerText = locationHost;
    document.getElementById("locationHref").innerText = locationHref;
  }