//This program finds and displays all tags of the current HTML page.

"use strict";
//debugger;

window.addEventListener("load", function () {
    displayTags();
  });


function displayTags() {

    let tags = document.getElementsByTagName("*");
    let allTags = "";

    for (let i = 0; i < tags.length; i++) {
      let tag = tags[i];
      allTags += tag.tagName + "\n";
    }

document.getElementById("tag").innerText = allTags;
}