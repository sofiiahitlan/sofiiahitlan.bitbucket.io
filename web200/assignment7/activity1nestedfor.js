//This program uses nested loop to generate a multiplication table. Start and end values are defined by the user.

//Reference: https://stackoverflow.com/questions/41465569/multiplication-table-in-javascript

"use strict";

//debugger;

main();

window.addEventListener("load", function () {
    document.getElementById("startValue").addEventListener("input", main);
    document.getElementById("endValue").addEventListener("input", main);
});

function main() {
    let startValue = getStartValue();
    let endValue = getEndValue();
    processTable(startValue, endValue);
}

function getStartValue(){
    let startValue = document.getElementById("startValue").value;
    //let startValue = 1;

    return startValue;
}

function getEndValue(){
    let endValue = document.getElementById("endValue").value;
    //let endValue = 5;

    return endValue;
}

function processTable(startValue, endValue) {
    let i, j;
    let multiplicationTable = "";
    //let tableHeader = "";

    //tableHeader += `<tr>`;
    for (i = startValue; i <= endValue; i++) {
        //tableHeader += `<td>${i}</td>`; 
        multiplicationTable += `<tr>`;
        for(j = startValue; j <= endValue; j ++){
            multiplicationTable += `<td>${i*j}<td>`;        
        }
    multiplicationTable += `</tr>`;   

    }
    //tableHeader += `</tr>`;

    document.getElementById("table").innerHTML = multiplicationTable;
    //document.getElementById("tableHeader").innerHTML = tableHeader;


}