//This program uses a for loop to generate a list of multiplication expressions for a value entered by user. 

"use strict";

//debugger;

main();

window.addEventListener("load", function () {
    document.getElementById("value").addEventListener("input", main);
    document.getElementById("expressionsNumber").addEventListener("input", main);
});

function main() {
    let value = getValue();
    let expressionsNumber = getExpressionsNumber();
    let expressions = processExpressions(value, expressionsNumber);
}

function getValue(){
    let value = document.getElementById("value").value;
    //let value = 2;

    return value;
}

function getExpressionsNumber(){
    let expressionsNumber = document.getElementById("expressionsNumber").value;
    //let  expressionsNumber = 2;

    return expressionsNumber;
}

function processExpressions(value, expressionsNumber){
    let expressions = "";
    for (let condition = 0; condition < Number(expressionsNumber); condition++){
        expressions += "\n\n" + value.toString() + " * " + condition.toString() + " = " + (value*condition).toString();
        document.getElementById("expressions").innerText = expressions;        
    }

}
