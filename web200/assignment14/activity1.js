//This program allows the user to enter information into the form. The input is then validated. 
//Get and post submit buttons are kept disabled until all data is successfully validated. If no, errors are being displayed.

//Reference: https://www.w3resource.com/javascript/form/email-validation.php

"use strict";


function User(firstName = null, lastName = null, address = null, city = null, state = null, zipCode = null,
              email = null, phoneNumber = null, dateOfBirth = null) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.address = address;
  this.city = city;
  this.state = state;
  this.zipCode = zipCode;
  this.email = email;
  this.phoneNumber = phoneNumber;
  this.dateOfBirth = dateOfBirth;
}


window.addEventListener("load", function () {
  let elements = document.getElementsByTagName("input");

  for (let i = 0; i < elements.length; i++) {
    elements[i].addEventListener("focus", inputFocus);
    elements[i].addEventListener("input", inputInput);
  }

  document.getElementById("get").addEventListener("click", getClick);
  document.getElementById("post").addEventListener("click", postClick);

  document.getElementById("firstName").focus();
});

function inputFocus() {
  document.activeElement.select();
  displayPrompt();
  checkButtons();
}

function displayPrompt(id = null) {
  const PROMPTS = {
    firstName: "First name has to be at least one character long.",
    lastName: "Last name has to be at least one character long.",
    address: "Enter your street address.",
    city: "Enter your city.",
    state: "Enter 2 letter abbreviation of your state.", 
    zipCode: "Enter your 5 digit zip code.",
    email: "Enter your email address in name@provider.com format",
    phoneNumber: "Enter your phone number in ###-###-#### format",
    dateOfBirth: "Enter your date of birth in ##/##/#### format",
    
  }

  let elements = document.getElementsByTagName("output");

  if (id == null) {
    id = document.activeElement.id;

    for (let i = 0; i < elements.length; i++) {
      elements[i].innerText = "";
    }
  }
  try {
    document.getElementById(id + "-prompt").innerText = PROMPTS[id];
  }
  catch {
    // ignore when the active element is a button
  }
}

  function inputInput() {
    let element = document.activeElement;
    let id = element.id;
  
  
    if (id == "phoneNumber") {
      checkPhonePattern();
    }
    if (!element.checkValidity()) {
      document.getElementById(id + "-prompt").innerText = element.validationMessage;
    }
    else {
      document.getElementById(id + "-prompt").innerText = "";
    }
  
    checkButtons();

    return element;
  }

  function checkPhonePattern() {
    let element = document.getElementById("phoneNumber");
    let value = element.value;
    
    if (value.length > 3 && value.substr(3, 1) != "-") {
      value = value.substr(0, 3) + "-" + value.substr(3);
    }
  
    if (value.length > 7 && value.substr(7, 1) != "-") {
      value = value.substr(0, 7) + "-" + value.substr(7);
    }
  
    if (element.value != value) {
      element.value = value;
    }
  }

  function checkButtons() {
    debugger;
    let elements = document.getElementsByTagName("input");
  
    for (let i = 0; i < elements.length; i++) {
      if (!elements[i].checkValidity()) {
        document.getElementById("get").disabled = true;
        document.getElementById("post").disabled = true;
        return;
      }
    }
  
    document.getElementById("get").disabled = false;
    document.getElementById("post").disabled = false;
  }
  
  function getClick() {
    let jsonData = "https://jsonplaceholder.typicode.com/users";
    let dataRequest = new XMLHttpRequest();
    dataRequest.open("GET", jsonData)
    dataRequest.onload = function() {
      document.getElementById("response").innerText = "status: " + dataRequest.status + "\n";
      document.getElementById("response").innerText += "responseText:\n" + dataRequest.responseText;
    };
    dataRequest.send(null);
  }
  
  function postClick() {
    let jsonData = "https://jsonplaceholder.typicode.com/users";
    let data = document.getElementById("data").innerText;
    let request = new XMLHttpRequest();
    request.open("POST", jsonData, true);
    request.onreadystatechange = function() {
    document.getElementById("response").innerText = "status: " + request.status + "\n";
    document.getElementById("response").innerText += "responseText:\n" + request.responseText;
  };
    request.send(data);

  }
