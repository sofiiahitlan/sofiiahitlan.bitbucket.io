//This program uses functions to calculates and displays weekly, monthly and annual gross pay. 
//User is required to enter his or her rate per hour and amount of hours he or she works every week.

"use strict";

debugger;

main();

window.addEventListener("load", function () {
    document.getElementById("rate").addEventListener("input", main);
    document.getElementById("hours").addEventListener("input", main);
});

function main() {
    let rate = getRate();
    let hours = getHours();
    let weekly = calculateWeekly(rate, hours);
    let monthly = calculateMonthly(weekly);
    let annual = calculateAnnual(weekly);
    displayResults(weekly, monthly, annual);
}

function getRate() {
    let rate = document.getElementById("rate").value;

    return rate;
}

function getHours() {
    let hours = document.getElementById("hours").value;

    return hours;
}

function calculateWeekly(rate, hours) {
    let weekly = rate * hours;

    return weekly;
}

function calculateMonthly(weekly) {
    let monthly = weekly * 4;
    
    return monthly;
}

function calculateAnnual(weekly) {
    let annual = weekly * 52;

    return annual;
}

function displayResults(weekly, monthly, annual) {
    document.getElementById("weekly").innerText = weekly.toFixed(2);
    document.getElementById("monthly").innerText = monthly.toFixed(2);
    document.getElementById("annual").innerText = annual.toFixed(2);
}