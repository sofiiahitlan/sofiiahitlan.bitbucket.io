//This program uses functions to calculate and display the amount of flooring needed for the room in square yards.
//User has to enter length and width of the room in feet.

"use strict";

debugger;

main();

window.addEventListener("load", function () {
    document.getElementById("length").addEventListener("input", main);
    document.getElementById("width").addEventListener("input", main);
});

function main(){
    let length = getLength();
    let width = getWidth();
    let squareFeet = calculateSquareFeet(length, width);
    let squareYards = calculateSquareYards(squareFeet);
    displayResult(squareYards);
}

function getLength(){
    let length = document.getElementById("length").value;

    return length;
}

function getWidth(){
    let width = document.getElementById("width").value;

    return width;
}

function calculateSquareFeet(length, width){
    let squareFeet = length * width;

    return squareFeet;
}

function calculateSquareYards(squareFeet){
    let squareYards = squareFeet / 9;

    return squareYards;
}

function displayResult(squareYards){
    document.getElementById("squareYards").innerText = squareYards.toFixed(2);
}