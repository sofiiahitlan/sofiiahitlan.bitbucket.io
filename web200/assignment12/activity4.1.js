//This program uses nested loop to generate a multiplication table. Start and end values are defined by the user.
//Reference: https://stackoverflow.com/questions/41465569/multiplication-table-in-javascript
//Rows and cell data are generated dynamically depending on the input values. 
//Cell data is first attached to the cell. Cell is then attached to the row. Row is then attached to the table.

"use strict";



main();

window.addEventListener("load", function () {
    document.getElementById("startValue").addEventListener("input", main);
    document.getElementById("endValue").addEventListener("input", main);
});

function main() {
    let startValue = getStartValue();
    let endValue = getEndValue();
    displayTable(startValue, endValue);
}

function getStartValue(){
    let startValue = document.getElementById("startValue").value;
    //let startValue = 1;

    return startValue;
}

function getEndValue(){
    let endValue = document.getElementById("endValue").value;
    //let endValue = 5;

    return endValue;
}

function displayTable(startValue, endValue) {
    
    let table = document.getElementById("table");

    while (table.lastChild) {
        table.removeChild(table.lastChild);
    }

    let i, j;
    for (i = startValue; i <= endValue; i++) {
        let newRow = document.createElement("tr");
        for(j = startValue; j <= endValue; j ++){
            let newCell = document.createElement("td");
            newRow.appendChild(newCell);
            let cellData = document.createTextNode(i*j);
            newCell.appendChild(cellData);
        }
    table.appendChild(newRow);
    }


}