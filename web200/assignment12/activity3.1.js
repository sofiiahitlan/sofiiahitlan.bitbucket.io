//This program uses a for loop to generate a list of multiplication expressions for a value entered by user. 
//Expressions are added to the array. A new paragraph is created for each array element (expression).
//All paragraphs are then attached to their parent and displayed.
//If user changes his/her mind and modifies the input values, previous paragraphs disappear, and new ones are being created and displayed.

"use strict";

//debugger;

main();

window.addEventListener("load", function () {
    document.getElementById("value").addEventListener("input", main);
    document.getElementById("expressionsNumber").addEventListener("input", main);
});

function main() {
    let value = getValue();
    let expressionsNumber = getExpressionsNumber();
    let expressions = processExpressions(value, expressionsNumber);
    expressions =  displayExpressions(expressions);
}

function getValue(){
    let value = document.getElementById("value").value;
    //let value = 5;

    return value;
}

function getExpressionsNumber(){
    let expressionsNumber = document.getElementById("expressionsNumber").value;
    //let  expressionsNumber = 9;

    return expressionsNumber;
}

function processExpressions(value, expressionsNumber){
    let expressions = [];
    for (let condition = 0; condition < Number(expressionsNumber); condition++) {
        expressions.push(value.toString() + " * " + condition.toString() + " = " + (value*condition).toString());
    }

    return expressions;      
}


function displayExpressions(expressions) {
    debugger;

    let paragraphParent = document.getElementById("outputDiv");

    while (paragraphParent.lastChild) {
        paragraphParent.removeChild(paragraphParent.lastChild);
    }
    
    for (let condition = 0; condition < expressions.length; condition++) {
        let newParagraph = document.createElement("p");
        let paragraphText = document.createTextNode(expressions[condition]);
        newParagraph.appendChild(paragraphText);
        paragraphParent.appendChild(newParagraph);
    }
}

