//This program uses conditions to calculate and display weekly gross pay. Hours over 40 are considered overtime and are paid at 1.5 rate.

"use strict";

debugger;

main();

window.addEventListener("load", function () {
    document.getElementById("rate").addEventListener("input", main);
    document.getElementById("hours").addEventListener("input", main);
});

function main() {
    let rate = getRate();
    let hours = getHours();
    let weeklyGross = calculateWeekly(rate, hours);
    displayResult(weeklyGross);
}

function getRate() {
    let rate = document.getElementById("rate").value;

    return rate;
}

function getHours() {
    let hours = document.getElementById("hours").value;

    return hours;
}

function calculateWeekly(rate, hours) {
    let weeklyGross = 0;

    if (hours <= 40) {
        weeklyGross = rate * hours;
    } else { 
        weeklyGross = (40 + (hours - 40) * 1.5) * rate;
    }

    return Number(weeklyGross);
}


function displayResult(weeklyGross) {
    document.getElementById("weeklyGross").innerText = weeklyGross.toFixed(2);
}
