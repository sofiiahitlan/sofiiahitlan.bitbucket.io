//This program uses switch statement to find out the size of the sock depending on the shoe size entered. Full and half sizes are accepted as an input.

"use strict";

debugger;

main();

window.addEventListener("load", function () {
    document.getElementById("shoeSize").addEventListener("input", main);
});

function main() {
    let shoeSize = getShoeSize();
    let sockSize = calculateSockSize(shoeSize);
    displayResult(sockSize);
}

function getShoeSize() {
    let shoeSize = document.getElementById("shoeSize").value;
    if (Number.isInteger(Number(shoeSize)) == false){
        shoeSize = Number(shoeSize) + 0.5;
    }
   
    return shoeSize;
}

function calculateSockSize(shoeSize){
    let sockSize; 

    switch (Number(shoeSize)) {
        case 1: case 2: case 3:
            sockSize = "extra small.";
            break;
        case 4: case 5: case 6:
            sockSize = "small.";
            break;
        case 7: case 8: case 9:
            sockSize = "medium.";
            break;
        case 10: case 11: case 12:
            sockSize = "large.";
            break;
        case 13: case 14: case 15: case 16:
            sockSize = "extra large.";
            break;
        default:
            sockSize = "unknown.";
    }

    return sockSize;
}

function displayResult(sockSize) {
    document.getElementById("sockSize").innerHTML = sockSize;
}

