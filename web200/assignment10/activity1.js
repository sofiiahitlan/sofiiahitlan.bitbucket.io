//This program aks user to input book details and formats them using APA and MLA styles. Both styles are displayed.

"use strict";
//debugger;
window.addEventListener("load", function () {
    document.getElementById("button").addEventListener("click", buttonClick);
});

function buttonClick() {
    let title = getTitle();
    let authorFirstName = getAuthorFirstName();
    let authorMiddleName = getAuthorMiddleName();
    let authorMiddleInitial = findAuthorMiddleInitial(authorMiddleName);
    let authorLastName = getAuthorLastName();
    let year = getYear();
    let publisher = getPublisher();
    let city = getCity();
    let state = getState();
    let citation = getCitation(title, authorFirstName, authorMiddleInitial, authorLastName, year, publisher, city, state);
    displayAPACitation(citation);
    displayMLACitation(citation);
}

function getTitle() {
    let title = document.getElementById("title").value;
    title = nameCasing(title);
    
    return title;
}

function getAuthorFirstName() {
    let authorFirstName = document.getElementById("authorFirstName").value;
    authorFirstName = nameCasing(authorFirstName);

    return authorFirstName;
}


function getAuthorMiddleName() {
    let authorMiddleName = document.getElementById("authorMiddleName").value;

    return authorMiddleName;
}

function findAuthorMiddleInitial(authorMiddleName) {
    let authorMiddleInitial = authorMiddleName[0].toUpperCase();

    return authorMiddleInitial;
}

function getAuthorLastName() {
    debugger;
    let authorLastName = document.getElementById("authorLastName").value;
    authorLastName = nameCasing(authorLastName);

    return authorLastName;
}

function getYear() {
    let year = document.getElementById("year").value;

    return year;
}

function getPublisher() {
    let publisher = document.getElementById("publisher").value;
    publisher = nameCasing(publisher);

    return publisher;
}

function getCity() {
    let city = document.getElementById("city").value;
    city = nameCasing(city);

    return city;
}

function getState() {
    let state = document.getElementById("state").value;

    if (state.length > 2) {
        state = nameCasing(state);
    } else {
        state = state.toUpperCase();
    }

    return state;
}

//debugger;
function getCitation(title, authorFirstName, authorMiddleInitial, authorLastName, year, publisher, city, state) {
    let Citation = new CitationConstructor();
    Citation.title = title;
    Citation.authorFirstName = authorFirstName;
    Citation.authorMiddleInitial = authorMiddleInitial;
    Citation.authorLastName = authorLastName;
    Citation.year = year;
    Citation.publisher = publisher;
    Citation.city = city;
    Citation.state = state;

    return Citation;
}

function displayAPACitation(Citation) {
    document.getElementById("APACitation").innerText = Citation.authorLastName + ", " + Citation.authorFirstName[0] + ". " + Citation.authorMiddleInitial + ". (" + Citation.year + ") " + Citation.title + ". " + Citation.city + ", " + Citation.state + ": " + Citation.publisher + ".";
}

function displayMLACitation(Citation) {
    document.getElementById("MLACitation").innerText = Citation.authorLastName + ", " + Citation.authorFirstName + " " + Citation.authorMiddleInitial + ". " + Citation.title + ". " + Citation.city + ": " + Citation.publisher + ", " + Citation.year + ". Print.";

}

function CitationConstructor(title = null, authorFirstName = null, authorMiddleInitial = null, authorLastName = null, year = null, publisher = null, city = null, state = null) {
    this.title = title;
    this.authorFirstName = authorFirstName;
    this.authorMiddleInitial = authorMiddleInitial;
    this.authorLastName = authorLastName;
    this.year = year;
    this.publisher = publisher;
    this.city = city;
    this.state = state;
}

function nameCasing(name) {
    let nameInitial = name[0].toUpperCase();
 
    for (let i = 1; i < name.length; i++) {
        nameInitial += name[i].toLowerCase();
        
    }
    name = nameInitial;

    return name;
}