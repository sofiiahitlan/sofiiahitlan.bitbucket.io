//This program allows the user to order pizza. It shows pizza total, tax, subtotal, and pizza details after pizza is added to the order.

"use strict";

const SIZEPRICING = {
  small : 4.99,
  medium : 6.99,
  large : 8.99,
}

const TOPPINGPRICE = 1.50;
const mediumToppingSurcharge = 1.20;
const largeToppingSurcharge = 1.25;

let order = null;

function Order(customer = null, pizzas = null) {
  this.customer = customer;
  this.pizzas = pizzas;
  if (pizzas == null) {
    this.pizzas = [];
  }

}

function Pizza(pizzaSize = null, pizzaCrust = null, pizzaSauce = null, pizzaTopping = null) {
  this.pizzaSize = pizzaSize;
  this.pizzaCrust = pizzaCrust;
  this.pizzaSauce = pizzaSauce;
  this.pizzaTopping = pizzaTopping;

  if (pizzaTopping == null) {
    this.pizzaTopping = [];
  }
}

function Customer(firstName = null, lastName = null, address = null, city = null, state = null, zipCode = null,
                  phoneNumber = null) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.address = address;
  this.city = city;
  this.state = state;
  this.zipCode = zipCode;
  this.phoneNumber = phoneNumber;
}

window.addEventListener("load", function () {
  let elements = document.getElementsByTagName("input");

  for (let i = 0; i < elements.length; i++) {
    elements[i].addEventListener("focus", inputFocus);
    elements[i].addEventListener("input", inputInput);
  }

  document.getElementById("firstName").focus();


  document.getElementById("addToOrder").addEventListener("click", addPizzaToOrder);
  

  document.getElementById("post").addEventListener("click", submitOrder);

  order = new Order();

});

function addPizzaToOrder() {
  let newPizza = new Pizza();

  let pizzaSize = document.getElementsByName("pizzaSize");
  //debugger;
  
  for (let i = 0; i < pizzaSize.length; i++) {
    
    if (pizzaSize[i].checked) {
      console.log(pizzaSize[i].value);
      newPizza.pizzaSize = pizzaSize[i].value;
    }
  }

  let pizzaCrust = document.getElementsByName("pizzaCrust");
  //debugger;
  
  for (let i = 0; i < pizzaCrust.length; i++) {
    
    if (pizzaCrust[i].checked) {
      console.log(pizzaSize[i].value);
      newPizza.pizzaCrust = pizzaCrust[i].value;
    }
  }

  let pizzaSauce = document.getElementsByName("pizzaSauce");
  
  for (let i = 0; i < pizzaSauce.length; i++) {
    
    if (pizzaSauce[i].checked) {
      console.log(pizzaSauce[i].value);
      newPizza.pizzaSauce = pizzaSauce[i].value;
    }
  }

  let pizzaTopping = document.getElementsByTagName("input");

  let toppingsNumber = 0;

  for (let i = 0; i < pizzaTopping.length; i++) {

    
    if (pizzaTopping[i].type == "checkbox" && pizzaTopping[i].checked) {
      console.log(pizzaTopping[i].value);
      newPizza.pizzaTopping.push(pizzaTopping[i].value);
      toppingsNumber += 1;

    }
  }

  console.log(newPizza);

  order.pizzas.push(newPizza);
debugger;
  getOrderTotal(newPizza, toppingsNumber);
}


function getOrderTotal(newPizza, toppingsNumber) {

  let orderSubtotal = 0.00;
  let pizzaCost = 0.00;
  let tax = 0.00;


  if (newPizza.pizzaSize == "small") {
    pizzaCost = pizzaCost + SIZEPRICING.small + toppingsNumber * TOPPINGPRICE;
  }
  if (newPizza.pizzaSize == "medium") {
    pizzaCost = pizzaCost + SIZEPRICING.medium + toppingsNumber * TOPPINGPRICE * mediumToppingSurcharge;
  }
  if (newPizza.pizzaSize == "large") {
    pizzaCost = pizzaCost + SIZEPRICING.large + toppingsNumber * TOPPINGPRICE * largeToppingSurcharge;
  } else { 
    console.log("Missing size, crust, sauce or ingrideints info");
  }



  console.log(pizzaCost);

//WILL FIX THIS ADD LOOP LATER (to get total of multiple pizzas)
  tax = pizzaCost * 0.1;
  console.log(tax);

  orderSubtotal = Number(pizzaCost) + Number(tax);
  console.log(orderSubtotal);

  let pizzaPreview = document.getElementById("pizzaPreview");
  while (pizzaPreview.lastChild) {
    pizzaPreview.removeChild(pizzaPreview.lastChild);
  }

  let totalsParagraph = document.createElement("p");
  let pizzaTotal = document.createTextNode("Pizza total: " + pizzaCost.toFixed(2) + "$. ");
  let pizzaTax = document.createTextNode("Pizza tax: " + tax.toFixed(2) + "$. ");
  let grandTotal = document.createTextNode("Order subtotal: " + orderSubtotal.toFixed(2) + "$. ");
  totalsParagraph.appendChild(pizzaTotal);
  totalsParagraph.appendChild(pizzaTax);
  totalsParagraph.appendChild(grandTotal);


  pizzaPreview.appendChild(totalsParagraph);
  // totalsParagraph.appendChild(pizzaTotal);

  // pizzaPreview.appendChild(pizzaTotal);

  let pizzaFeatures = ["Size: ", "Crust: ", "Sauce: ", "Toppings: "];


  // debugger;

      let condition = 0;
      let newParagraph = document.createElement("p");
      let sizePreview = document.createTextNode(pizzaFeatures[condition] + newPizza.pizzaSize + ", ");
      let crustPreview = document.createTextNode(pizzaFeatures[condition + 1] + newPizza.pizzaCrust + ", ");
      let saucePreview = document.createTextNode(pizzaFeatures[condition + 2] + newPizza.pizzaSauce + ", ");
      let toppingsPreview = document.createTextNode(pizzaFeatures[condition + 3] + " ");

      newParagraph.appendChild(sizePreview);
      newParagraph.appendChild(crustPreview);
      newParagraph.appendChild(saucePreview);
      newParagraph.appendChild(toppingsPreview);
      
      pizzaPreview.appendChild(newParagraph);
      


      for (let condition = 0; condition < newPizza.pizzaTopping.length; condition++) {

      let toppingsListedPreview = document.createTextNode(newPizza.pizzaTopping[condition] + ", ");

      newParagraph.appendChild(toppingsListedPreview);

      pizzaPreview.appendChild(newParagraph);

      }
 
  //return pizzaCost;
}


function inputFocus() {
  document.activeElement.select();
  displayPrompt();
  checkButtons();
}

function displayPrompt(id = null) {
  const PROMPTS = {
    firstName: "First name has to be at least one character long.",
    lastName: "Last name has to be at least one character long.",
    address: "Enter your street address.",
    city: "Enter your city.",
    state: "Enter 2 letter abbreviation of your state.", 
    zipCode: "Enter your 5 digit zip code.",
    phoneNumber: "Enter your phone number in ###-###-#### format",    
  }

  let elements = document.getElementsByTagName("output");

  if (id == null) {
    id = document.activeElement.id;

    for (let i = 0; i < elements.length; i++) {
      elements[i].innerText = "";
    }
  }
  try {
    document.getElementById(id + "-prompt").innerText = PROMPTS[id];
  }
  catch {
    // ignore when the active element is a button
  }
}

  function inputInput() {
    let element = document.activeElement;
    let id = element.id;

    if (id == "") {
      return;
    }
  
    if (id == "phoneNumber") {
      checkPhonePattern();
    }
    if (!element.checkValidity()) {
      document.getElementById(id + "-prompt").innerText = element.validationMessage;
    }
    else {
      document.getElementById(id + "-prompt").innerText = "";
    }
  
    checkButtons();

    return element;
  }

  function checkPhonePattern() {
    let element = document.getElementById("phoneNumber");
    let value = element.value;
    
    if (value.length > 3 && value.substr(3, 1) != "-") {
      value = value.substr(0, 3) + "-" + value.substr(3);
    }
  
    if (value.length > 7 && value.substr(7, 1) != "-") {
      value = value.substr(0, 7) + "-" + value.substr(7);
    }
  
    if (element.value != value) {
      element.value = value;
    }
  }

  function checkButtons() {
    let elements = document.getElementsByTagName("input");
  
    for (let i = 0; i < elements.length; i++) {
      if (!elements[i].checkValidity()) {
        document.getElementById("post").disabled = true;
        return;
      }
    }
  
    document.getElementById("post").disabled = false;
  }
  


  function submitOrder() {
    //debugger;

    let orderingCustomer = new Customer();
    orderingCustomer.firstName = document.getElementById("firstName").value;
    orderingCustomer.lastName = document.getElementById("lastName").value;
    orderingCustomer.address = document.getElementById("address").value;
    orderingCustomer.city = document.getElementById("city").value;
    orderingCustomer.state = document.getElementById("state").value;
    orderingCustomer.zipCode = document.getElementById("zipCode").value;
    orderingCustomer.phoneNumber = document.getElementById("phoneNumber").value;
    
    order.customer = orderingCustomer;

    console.log(orderingCustomer);

    let jsonData = "https://jsonplaceholder.typicode.com/users";
    let data = document.getElementById("data").innerText;
    let request = new XMLHttpRequest();
    request.open("POST", jsonData, true);
    request.onreadystatechange = function() {
    document.getElementById("response").innerText = "status: " + request.status + "\n";
    document.getElementById("response").innerText += "responseText:\n" + request.responseText;
  };
    request.send(data);

  }

