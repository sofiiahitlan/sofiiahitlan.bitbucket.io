// This profram demonstrate various operations and converting between data types.

"use strict";

let highestWorld = 136.1;
let coldestWorld = -126;
let highestHawaii = "98";
let coldestHawaii = 12;
let highest2019 = null;
let coldest2019;
let temperatureScale = " F.";


let hawaiiSumWrong =  highestHawaii  + coldestHawaii;
console.log(`highesthawaii data type: ${typeof highestHawaii}`);
let hawaiiSumRight =  Number(highestHawaii) + coldestHawaii; 
console.log(`highesthawaii data type: ${typeof highestHawaii}`);
let hawaiiDifference = highestHawaii.toString() - coldestHawaii;
console.log(`highesthawaii data type: ${typeof highestHawaii}`);

let hawaiiResultWrong = "JavaScript evaluates expressions from left to right. In this particular case the highest Hawaii temparature is a string, the lowest one is a number. After the Java Script sees that the first variable is the string, it will assume that the second one is or should be a string as well. for the purpose of this assignment lets assume we need to get a sum of the highest and the lowest Hawaii temperature for the future calculations. Concatenation will happen here instead of the addition: \"98\" + 12 = " +  hawaiiSumWrong + temperatureScale;
let hawaiiResultRight = "We can get the right result if we convert the highest temperature to number:  98 + 12 = " + hawaiiSumRight + temperatureScale;
let hawaiiDifferenceResult = "Please note that type coercion is done automatically during substraction: \"98\" - 12 = " + hawaiiDifference + temperatureScale;

document.getElementById("hawaiiResultWrong").innerText = hawaiiResultWrong;
document.getElementById("hawaiiResultRight").innerText = hawaiiResultRight;
document.getElementById("hawaiiDifferenceResult").innerText = hawaiiDifferenceResult;