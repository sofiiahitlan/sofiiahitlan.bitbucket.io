//This program defines various string literals.
// It does convertion between string and numeric data types using Number() and .toFixed() methods.
// Variables are concatenated with literals.

"use strict";

var book = 'The title of the book is "Where the crawdads sing" by Delia Owens. '
var review = "It's the bestseller of this week. "
var copiesSold = "1.1012";

copiesSold = Number(copiesSold);
var copiesFixed = copiesSold.toFixed(2);

var result = book + review + "More than " + copiesFixed + " million print copies of the book were sold since 2018.";

document.getElementById("results").innerText = result;

