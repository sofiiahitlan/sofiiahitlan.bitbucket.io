Sofiia Hitlan

# Week 1
The reason why I'm taking WEB200 is that this class is required for the web development degree I'm pursuing. Every web developer must know HTML, CSS, and JS. This trio makes every web page complete.
I intend to incorporate JS into my projects to make the web pages interactive. Nowadays users get pickier, so web developers shouldn't fall behind and think a little bit harder (and more creative) to make websites more attractive. Interactivity may help to bring more traffic in. More traffic means happy customers. 

# Week 2
This week I learned that to create a variable and assign a value to it, a variable type doesn't have to be mentioned. There are fewer types in JS than in other programming languages which makes the learning experience less frustrating. Also, JavaScript can distinguish addition from concatenation. It's not a logical error to concatenate a string to the number.
I was pleasantly surprised to see that JS has strict comparison operators === and !==. 
These JavaScript features make me think that the language is pretty flexible and is built to help web developers prevent logical errors more successfully. 
Knowing these basics makes me one step closer to getting my first web developer job. 

# Week 3 
This week I learned that JavaScript has an automatic type coercion. If the data type of the variable is wrong it will try to make it right without displaying an error. I've practiced using strict directives. I'll keep using them in the future to make sure the mistakes I make during the coding are not left uncaught. It may save my or someone else's times during the maintenance stage. I've discovered basic String and Number methods. These methods will help me to manipulate numbers and string in all possible ways to create better responsive websites. 

# Week 4 
This week I learned that functions are defined using the "function" keyword. While functions have to be called upon to be executed, function expressions can invoke themselves. I learned about the existence of anonymous functions and that they are meant to be executed only once. Named function expressions allow anonymous function recursion. I found out that JavaScript doesn't check the number of arguments passed to the function. Knowing this I'll consider adding default values to parameters to avoid future problems. I find the minimum and maximum math functions very helpful and will be using them to create less cluttered code.

# Week 5
This week I learned that elements could change their properties with the help of events.  Events occur in the time defined by the developer. They can happen while mousing over the element, after the webpage has loaded, during or after clicking on the element, etc. The event handler is a block of code that runs when the event happens. JavaScript is not complete without events. Events should be used by programmers because without them the page won’t be as interactive as it should be. I’ll keep adding events to the code to make my future projects more responsive and intuitive.

# Week 6
This week I learned that switch conditional statement exists in JavaScript just like in C++. I'll be using a switch structure when the list of conditions to be tested is pretty long because it improves the readability and makes code more organized and clear. I discovered the differences between the AND logical operator && and &, and between OR logical operator and || and |. I'll be using them with caution to avoid getting unexpected results.

# Week 7 
This week I learned the purpose of nested loops. I found out that the outer loop will run only once while the inner loop will run n times depending on the defined n value. After that, the outer loop will run again. At this point, I find nested loops harder to read, understand, and implement. Overall, I'll be using loops whenever repetition of any action is needed. I'll be implementing them in my code to keep it DRY.

# Week 8
This week I learned that push, pop, shift, and unshift methods are used to add elements to arrays.
Array.isArray() method is used to find out if the object is an array. It’s especially helpful for programmers who work with someone else’s code and need to clarify it.
I learned that arrays have a lot of methods, and one can be used instead of another. It gives programmers a chance to pick a few favorite methods and use them for any operations with arrays. Instead of knowing a little bit about each method, I think it’s better to master a few methods instead. It's my goal.

# Week 9
This week I learned that a date is a JavaScript object. The date object is static, but a timer interval can be added to make the date display dynamically. The ISO standard (YYYY-MM-DD) is the preferred JavaScript date input format. Since not all browsers know how to handle short and long dates correctly, I'll try to avoid them. I learned that the weekday and month countings in JS start from 0, not from 1. With the help of arrays, weekday and month numbers can be displayed as strings so that users can easily read them.

# Week 10
This week I learned that objects are sets of related data.  JS object holds properties and their values. Properties can be accessed using their names. While writing code, I've run into scope issues and learned that constructors don't belong inside the main function. They must be global. ﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿Data inside the object can be manipulated and displayed in different ways (MLA and APA styles, for example). Knowing this will help me to avoid having two almost similar objects in my code. I'll use this knowledge to write DRY code. 

# Week 11
This week I learned that HTML DOM is used for manipulation with HTML elements, attributes, and text. With its help, they can be accessed, changed, added, and deleted.  The window object model is in charge of page redirecting and works against URL.

# Week 12
This week I learned that it's not enough just to create a node in the JavaScript document. Each element node, except for the root element, has to be appended to its parent so that the parent-child relationship is established. Three main node types are an attribute node, an element node, and a text node. The nodeType property can be used to find the node type. The text inside the text node is its value and doesn't exist without the node. Elements could be added to the document or removed from it (with the help of remove() method). I'll be using dynamic HTML in my projects to improve the interaction with the customer. If the user is allowed to update input values, then I'll be using DHTML to get rid of the previously displayed results. HTML can be used to display the current results only.

# Week 13
This week I learned that input validation improves user experience. JavaScript can display errors before the form is submitted so that the user doesn’t have to do multiple submissions. Error messages indicate what form input requirements were not met.

# Week 14
This week I learned that AJAX is a technique that allows the data to be read from a web server without reloading the page. Also, AJAX makes it possible for the data to be sent to the server in the background. Even though X in AJAX stands for XML, JSON replaced XML and now is an industry-standard data format. JSON is a syntax for storing and exchanging data. JSON data is plain text, which makes it language independent. I’ll be using JSON for my future projects because it allows to nest objects inside the objects and arrays inside arrays. JSON’s syntax helps to organize the data and is easy to read.

#Final Exam
This semester I was introduced to JavaScript, and over time I was able to compare JS's features to features of other languages. JS is used in complex with HTML and CSS to create interactive web pages. One of the hardest parts was to understand how JS and HTML files work together and how to interchangeably use data from two files.