<!DOCTYPE html>

<html lang = "en">

<!--

Sofia Hitlan

WEB 235

Final project - Confirmation

03/30/2020

 -->

<head>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Rapid Home Services - Confirmation</title>

    <link href="../js3e-master/_css/jquery-ui.min.css" rel="stylesheet">

    <link href="site.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display+SC:400,400i,700,700i&display=swap" rel="stylesheet">



    <script src="site.js"></script> 

  </head>

<body>

  <header>

    <a href="#maincontent">Skip to main content</a>

  </header>

    <nav aria-labelledby="mainmenulabel" id ="mainnav">

      <h2 id="mainmenulabel" class="visuallyhidden">Main menu</h2>

      <ul>

        <li><a href = "#maincontent" aria-current="page">Home</a></li>

        <li><a href = "about.html">Services & Prices</a></li>

        <li><a href = "areas.html">Areas Served</a></li>

        <li><a href = "about.html">About Us</a></li>

        <li><a href = "gallery.html">Gallery</a></li>

        <li><a href = "testimonials.html">Testimonials</a></li>

        <li><a href = "contact.html">Contact Us</a></li>



      </ul>

    </nav>

    <main id = "maincontent">

      <article>

          <h1>Confirmation</h1>

          <?php

            // ini_Set('display_errors', 1);

            // error_reporting(E_ALL);

            //$localtime = localtime();

            //$datetime = (1900 + $localtime[5]) . "-" . (1 + $localtime[4])  . "-" . $localtime[3] . " " . $localtime[2] . ":" . $localtime[1];

            $name = $_POST["name"];
            $phone = $_POST["phone"];
            $email = $_POST["email"];
            $preference = $_POST["preference"];
            $comments = $_POST["comments"];

            $mail_message='
            NAME: '.$name.'
            PHONE NUMBER: '.$phone.'
            E-MAIL ADDRESS: '.$email.'
            PREFERRED CONTACT METHOD: '.$preference.'
            COMMENTS: '.$comments.'
            ';

            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/orders/Order_'.date("j_m_Y H-i-s").'.html', $mail_message);

            //Мыло для приёма заявокe
            $address = "coni4ca@gmail.com";

            $sub = 'Order on myrapidfix.com';
            $email = 'Myrapidfix';
            mail($address, $sub, $mail_message, "Content-type:text/plain; charset = utf-8\r\nFrom:$email");
          ?>

          <p><?php echo "Thank you, $name, for your contact request. We'll get back to you within 24 hours."?></p>

          <p><img src="images/sent.gif" alt=""></p>



      </article>

    </main>

    <footer>

      <p>Rapid Home Services © 2020</p>

    </footer>

  </body>

</html>